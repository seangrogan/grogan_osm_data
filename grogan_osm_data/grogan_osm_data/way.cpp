/*
	A way is an ordered list of nodes which normally also has at least one tag or is included within a Relation. A way can have between 2 and 2000 nodes, although it's possible that faulty ways with zero or a single node exist. A way can be open or closed. A closed way is one whose last node on the way is also the first on that way. A closed way may be interpreted either as a closed polyline, or an area, or both.
*/
#include "way.h"

way::way(){
}


way::~way(){
}

void way::parse_xml_element_way(tinyxml2::XMLElement * e) {
	id = std::stoi(e->Attribute("id"));
	lat = std::stod(e->Attribute("lat"));
	lon = std::stod(e->Attribute("lon"));

	version = std::stoi(e->Attribute("version"));
	changeset = std::stoi(e->Attribute("changeset"));
	user = e->Attribute("user");
	uid = std::stoi(e->Attribute("uid"));

	std::string vis_temp = e->Attribute("visible");
	visible = true;
	if (vis_temp == "false") { visible = false; }

	timestamp_string = e->Attribute("timestamp");

	parse_nodes(e);
	parse_way_tags(e);
}
/*	The following function will take the XMLElement and atempt to parse the elemet directly
	@parm tinyxml2::XMLElement * e - A XMLElement that contains the "tag" identifier
*/
void way::parse_way_tags(tinyxml2::XMLElement* e) {
	for (tinyxml2::XMLElement* s = e->FirstChildElement("tag"); s != NULL; s = s->NextSiblingElement("tag")) {
		std::string key = s->Attribute("k");
		std::string val = s->Attribute("v");
		tags[key] = val;
	}
}
/*	The following function will take the XMLElement and atempt to parse the elemet directly
	@parm tinyxml2::XMLElement * e - A XMLElement that contains the "nd" (node) identifier
*/
void way::parse_way_tags(tinyxml2::XMLElement* e) {
	for (tinyxml2::XMLElement* s = e->FirstChildElement("nd"); s != NULL; s = s->NextSiblingElement("nd")) {
		unsigned long long int id = std::stoi(s->Attribute("ref"));
		node * n;
		nd[id] = n;
	}
}