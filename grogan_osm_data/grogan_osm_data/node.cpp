/*
	A node is one of the core elements in the OpenStreetMap data model. It consists of a
	single point in space defined by its latitude, longitude and node id.

	A third, optional dimension (altitude) can also be included: key:ele (abrev. for
	"elevation"). A node can also be defined as part of a particular layer=* or level=*,
	where distinct features pass over or under one another; say, at a bridge.

	Nodes can be used to define standalone point features, but are more often used to
	define the shape or "path" of a way.
*/
#include "node.h"


/*	A constructor class for a node.  	
*/
node::node(){
}


node::~node(){
}

/*	The following function will take the XMLElement and atempt to parse the elemet directly 
	@parm tinyxml2::XMLElement * e - A XMLElement that contains the "node" identifier
*/
void node::parse_xml_element_node(tinyxml2::XMLElement * e){
	id = std::stoi(e->Attribute("id"));
	lat = std::stod(e->Attribute("lat"));
	lon = std::stod(e->Attribute("lon"));

	version = std::stoi(e->Attribute("version"));
	changeset = std::stoi(e->Attribute("changeset"));
	user = e->Attribute("user");
	uid = std::stoi(e->Attribute("uid"));
	
	std::string vis_temp = e->Attribute("visible");
	visible = true;
	if (vis_temp == "false") { visible = false;	}
	
	timestamp_string = e->Attribute("timestamp");

	parse_node_tags(e);
}

/*	The following function will take the XMLElement and atempt to parse the elemet directly
	@parm tinyxml2::XMLElement * e - A XMLElement that contains the "tag" identifier
*/
void node::parse_node_tags(tinyxml2::XMLElement* e) {
	for (tinyxml2::XMLElement* s = e->FirstChildElement("tag"); s != NULL; s = s->NextSiblingElement("tag")) {
		std::string key = s->Attribute("k");
		std::string val = s->Attribute("v");
		tags[key] = val;
	}
}
