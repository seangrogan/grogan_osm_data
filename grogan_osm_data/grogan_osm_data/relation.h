﻿#pragma once

#include <map>
#include <string>
#include <time.h>
#include "node.h"

/*
	A relation is one of the core data elements that consists of one or more tags and also an ordered list of one or more nodes, ways and/or relations as members which is used to define logical or geographic relationships between other elements. A member of a relation can optionally have a role which describes the part that a particular feature plays within a relation.
*/

class relation{
public:
	relation();
	~relation();
protected:
	unsigned long long int id;					// A 64 bit integer number >= 1.  
	double lat;									// Latitude; decimal number >= −90.0000000 and <= 90.0000000 with 7 decimal places
	double lon;									// Longitude; decimal number >= −180.0000000 and <= 180.0000000 with 7 decimal places
	int version;								// The edit version of the object. Newly created objects start at version 1 and the value is incremented by the server when a client uploads a new version of the object.
	unsigned long long int changeset;			// The changeset number in which the object was created or updated
	std::string user;							// The display name of the user who last modified the object
	int uid;									// The numeric identifier of the user who last modified the object. An user identifier never changes.
	bool visible;								// Whether the object is deleted or not in the database, if visible="false" then the object should only be returned by history calls.
	clock_t timestamp;							// Time of the last modification
	std::string timestamp_string;				// Time of the last modification (e.g. "2016-12-31T23:59:59.999Z").
	std::map<unsigned long long int, node*> member;	// the references to its nodes in each way
	std::map<std::string, std::string> tags;	// A set of key/value pairs, with unique key
};

