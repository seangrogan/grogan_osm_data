#pragma once
#include <string>
#include <map>

class tags
{
public:
	tags();
	~tags();
	void add_tag(std::string k, std::string v);
	std::string find_value(std::string k);
	std::string find_key(std::string v);
private:
	std::map<std::string, std::string> _tags;
};

