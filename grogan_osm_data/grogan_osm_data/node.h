﻿#pragma once

#include <string>
#include <map>
#include <time.h>
#include <stdio.h>
#include <locale.h>
#include <time.h>

#include "TinyXML2\tinyxml2.h"

/*
	A node is one of the core elements in the OpenStreetMap data model. It consists of a single point in space defined by its latitude, longitude and node id.

	A third, optional dimension (altitude) can also be included: key:ele (abrev. for "elevation"). A node can also be defined as part of a particular layer=* or level=*, where distinct features pass over or under one another; say, at a bridge.

	Nodes can be used to define standalone point features, but are more often used to define the shape or "path" of a way.
*/
class node{
public:
	node();
	~node();
	void parse_xml_element_node(tinyxml2::XMLElement* e);
	void parse_node_tags(tinyxml2::XMLElement* e);
protected:
	unsigned long long int id;					// A 64 bit integer number >= 1.  
	double lat;									// Latitude; decimal number >= −90.0000000 and <= 90.0000000 with 7 decimal places
	double lon;									// Longitude; decimal number >= −180.0000000 and <= 180.0000000 with 7 decimal places
	int version;								// The edit version of the object. Newly created objects start at version 1 and the value is incremented by the server when a client uploads a new version of the object.
	unsigned long long int changeset;			// The changeset number in which the object was created or updated
	std::string user;							// The display name of the user who last modified the object
	int uid;									// The numeric identifier of the user who last modified the object. An user identifier never changes.
	bool visible;								// Whether the object is deleted or not in the database, if visible="false" then the object should only be returned by history calls.
	clock_t timestamp;							// Time of the last modification
	std::string timestamp_string;				// Time of the last modification (e.g. "2016-12-31T23:59:59.999Z").
	std::map<std::string, std::string> tags;	// A set of key/value pairs, with unique key
};

