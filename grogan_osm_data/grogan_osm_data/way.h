﻿#pragma once

#include <string>
#include <map>
#include <time.h>
#include <stdio.h>
#include <locale.h>
#include <time.h>
#include "node.h"

/*
	A way is an ordered list of nodes which normally also has at least one tag or is included within a Relation. A way can have between 2 and 2000 nodes, although it's possible that faulty ways with zero or a single node exist. A way can be open or closed. A closed way is one whose last node on the way is also the first on that way. A closed way may be interpreted either as a closed polyline, or an area, or both.
*/
class way{
public:
	way();
	~way();
	void parse_xml_element_way(tinyxml2::XMLElement* e);
	void parse_way_tags(tinyxml2::XMLElement* e);
	void parse_nodes(tinyxml2::XMLElement* e);
protected:
	unsigned long long int id;					// A 64 bit integer number >= 1.  
	double lat;									// Latitude; decimal number >= −90.0000000 and <= 90.0000000 with 7 decimal places
	double lon;									// Longitude; decimal number >= −180.0000000 and <= 180.0000000 with 7 decimal places
	int version;								// The edit version of the object. Newly created objects start at version 1 and the value is incremented by the server when a client uploads a new version of the object.
	unsigned long long int changeset;			// The changeset number in which the object was created or updated
	std::string user;							// The display name of the user who last modified the object
	int uid;									// The numeric identifier of the user who last modified the object. An user identifier never changes.
	bool visible;								// Whether the object is deleted or not in the database, if visible="false" then the object should only be returned by history calls.
	clock_t timestamp;							// Time of the last modification
	std::string timestamp_string;				// Time of the last modification (e.g. "2016-12-31T23:59:59.999Z").
	std::map<unsigned long long int, node*> nd;	// the references to its nodes in each way
	std::map<std::string, std::string> tags;	// A set of key/value pairs, with unique key
};

